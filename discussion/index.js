// Javascript Synchronous and Asynchronous
// JS is by default synchronous, it means that only one statement is executed at a time

/*console.log('Hello world')
consol.log('Hello world again')
console.log('Byebye')*/

/*console.log('Hello world')
for(i = 0; i<=1000; i++){
	console.log(i)
}

console.log('Hello again')*/

//Asynchronous means that we can proceed to execute other statements/code block, while time consuming code is running in the background

// Getting All Posts
// The Fetch API allows you to asynchronously request for a resources(data).
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value.

/*
	Syntax:
		fetch("URL");
*/

// A promise may be in one of 3 possible states: fullfilled, rejected, or pending
console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

/*
	Syntax:
		fetch("URL")
		.then((response)=>{//line code})
*/

fetch("https://jsonplaceholder.typicode.com/posts")
// By using the response.status we only check if the promise is fulfilled or rejected
// ".then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"
//.then((response) => console.log(response.status))


// used the "json" method from the "response" object to convert tha data retrieved into JSON format to be used in our application.
//.then((response) => response.json())
//.then((json) => console.log(json))

//Display each title of the post
/*.then((json) => {
	json.forEach(posts => console.log(posts.title))
})*/

async function fetchData(){

	// await - waits for the "fetch" method to complete, then it stores the value in the result variable
	let result = await fetch ("https://jsonplaceholder.typicode.com/posts")

	// result returned by fetch is a promise returned
	console.log(result)
	// the returned "response" is an object
	console.log(typeof result)
	// we cannot access the content of the "response" by directly accessing it's body property
	console.log(result.body)


	// 
	let json = await result.json()
	console.log(json)
}
fetchData();

// Getting a specific post
// (retrieve, /posts/:id, GET)

// ":id" is a wildcard where you can put the unique identifier as value to show special resources
fetch("https://jsonplaceholder.typicode.com/posts/10")
.then((response) => response.json())
.then((json) => console.log(json))

// Creating a post
/*
	Syntax:
		fetch("URL", options)
		.then((response) => {line code})
		.then((json) => {line code})
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	
	method: "POST", //HTTP Method
	headers: {
		"Content-Type": "application/json"
	}, // Specifies the content that it will pass in JSON format
	body: JSON.stringify({
		title: "New Post",
		body: "Hello Batch 197",
		userId: 1
	}) // Sets the content/bpdy data of the "request" object to be sent to the backend/server
})
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))

// updating a post
// (update, /posts/:id, PUT)


// PUT method is used to update the whole 
fetch("https://jsonplaceholder.typicode.com/posts/10", {	
	method: "PUT", 
	headers: {
		"Content-Type": "application/json"
	}, 
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello again Batch 197",
		userId: 1
	}) 
})
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/10", {	
	method: "PATCH", 
	headers: {
		"Content-Type": "application/json"
	}, 
	body: JSON.stringify({
		title: "Corrected Post"
	}) 
})
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))

//Deleting post
//(delete, /posts/:id, DELETE)

fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "DELETE",
})















