async function retrieveTodos(){

	let response = await fetch('https://jsonplaceholder.typicode.com/todos');
	let resJson = await response.json();
	console.log(resJson);

	let allTitles = resJson.map((json)=> 'title: '+ json.title);
	console.log(allTitles);
}
retrieveTodos();

// 5
fetch('https://jsonplaceholder.typicode.com/todos/10')
.then((res)=>res.json())
.then((json)=>console.log(json));

//6
fetch('https://jsonplaceholder.typicode.com/todos/10')
.then((res)=>res.json())
.then((json)=>{
    console.log(`title: ${json.title} | status: ${json.status}`);
});

//7
async function createTodo(){
    let response = await fetch('https://jsonplaceholder.typicode.com/todos',
    {
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            userId: 6,
            status: true,
            title: "New Post"
        })
    });
    
    let resJson = await response.json();
    console.log(resJson);
}
createTodo();

//8
async function updateTodo(){
    let response = await fetch('https://jsonplaceholder.typicode.com/todos/10',
    {
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is an updated to do"
        })
    });
    
    let resJson = await response.json();
    console.log(resJson);
}
updateTodo();

//9
async function changeData(){
    let response = await fetch('https://jsonplaceholder.typicode.com/todos/10',
    {
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is a restructured to do",
            description: "new description",
            stat: "completed",
            date_completed: '2022-08-26',
            userId: 999999
        })
    });
    
    let resJson = await response.json();
    console.log(resJson);
}
changeData()

//10
async function patchTodo(){
    let response = await fetch('https://jsonplaceholder.typicode.com/todos/10',
    {
        method:"PATCH",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is a patched to do",
        })
    });
    
    let resJson = await response.json();
    console.log(resJson);
}
patchTodo();

//11
async function changeStatus(){
    let response = await fetch('https://jsonplaceholder.typicode.com/todos/10',
    {
        method:"PATCH",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            completed: true,
            date_completed: '2022-08-26'
        })
    });
    
    let resJson = await response.json();
    console.log(resJson);
}
changeStatus();

//12
async function deleteTodo(){
    let response = await fetch('https://jsonplaceholder.typicode.com/todos/10',
    {
        method:"DELETE"
    });
    
    let resJson = await response.json();
    console.log(resJson);
    console.log('method deleted');
}
deleteTodo();